<?php

namespace App\Repository;

use App\Entity\TypeLogement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeLogement>
 *
 * @method TypeLogement|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeLogement|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeLogement[]    findAll()
 * @method TypeLogement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeLogementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeLogement::class);
    }

    public function add(TypeLogement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TypeLogement $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    #region IDS for Types
    public function findMobilHomeId(): array
    {
        $arr = [];

        for ($i = 1; $i <= 4; $i++) {

            $arr[] = $this->createQueryBuilder('m')
                ->andWhere('m.id = :i')
                ->setParameter('i', $i)
                ->getQuery()->getResult();
        }


        return $arr;

    }

    public function findCaravaneId()
    {
        $arr = [];
        for ($i = 5; $i <= 7; $i++) {

            $arr[] = $this->createQueryBuilder('m')
                ->andWhere('m.id = :i')
                ->setParameter('i', $i)
                ->getQuery()->getResult();
        }

        return $arr;
    }


    public function findEmplacementId()
    {
        $arr = [];
        for ($i = 8; $i <= 9; $i++) {

            $arr[] = $this->createQueryBuilder('m')
                ->andWhere('m.id = :i')
                ->setParameter('i', $i)
                ->getQuery()->getResult();
        }

        return $arr;
    }
#endregion

//    /**
//     * @return TypeLogement[] Returns an array of TypeLogement objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TypeLogement
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
