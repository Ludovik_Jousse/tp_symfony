<?php

namespace App\DataFixtures;

use App\Entity\Facture;
use App\Entity\LigneDeFacturation;
use App\Entity\Logement;
use App\Entity\Reservation;
use App\Entity\Tarifs;
use App\Entity\TypeLogement;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->pass_hasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {

        $faker = Factory::create('fr_FR');

        $tarifs = [
            "1" => "15",
            "2" => "15",
            "3" => "15",
            "4" => "15",
            "5" => "15",
            "6" => "15",
            "7" => "15",
            "8" => "15",
            "9" => "15",
            "TSEnfant" => "0.35",
            "TSAdulte" => "0.60",
            "PEnfant" => "1",
            "PAdulte" => "1.50",
        ];


        foreach ($tarifs as $k => $v) {

            $tarif = new Tarifs();
            $tarif->setNom($k)
                ->setPrix($v);
            $manager->persist($tarif);
            $manager->flush();

        }


        #region UTILISATEURS
        $userOne = new User();

        $userOne->setFirstName('Lapipe')
            ->setLastName('Enzo')
            ->setEmail('admin@gmail.com')
            ->setPassword($this->pass_hasher->hashPassword($userOne, "admin"))
            ->setRole('ROLE_ADMIN');
        $this->addReference('user-admin', $userOne);
        $manager->persist($userOne);

        for ($i = 1 ; $i<=20 ; $i++){

            $user = new User();
            $user->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword($this->pass_hasher->hashPassword($user, "123"))
                ->setRole('ROLE_USER');
            $this->addReference('user-'. $i , $user);
            $manager->persist($user);

        }


        #endregion

        #region LOGEMENTS
        $prix = ['20', '24', '27', '34', '15', '18', '24', '12', '14'];

        for ($i = 1; $i <= 9; $i++) {
            $type = new TypeLogement();
            $type->setPrice($prix[$i - 1]);
            $type->setName($faker->name);
            $this->addReference('type-' . $i, $type);
            $manager->persist($type);
        }

        for ($i = 1; $i <= 20; $i++) {
            $logement = new Logement();
            $logement->setTypeLogement($this->getReference('type-' . rand(1, 4)))
                ->setUserId($this->getReference('user-admin'))
                ->setDescription($faker->paragraph(5, true))
                ->setFileName('default_mh.png')
                ->setUpdateAt(new \DateTime('now'))
                ->setName($faker->name);
            $this->addReference('logement-' . ($i), $logement);
            $manager->persist($logement);
        }


        for ($i = 1; $i <= 30; $i++) {
            $logement = new Logement();
            $logement->setTypeLogement($this->getReference('type-' . rand(1, 4)))
                ->setUserId($this->getReference(('user-' . rand(1,20))))
                ->setDescription($faker->paragraph(5, true))
                ->setFileName('default_mh.png')
                ->setUpdateAt(new \DateTime('now'))
                ->setName($faker->name);
            $this->addReference('logement-' . ($i + 20), $logement);
            $manager->persist($logement);
        }


        for ($i = 1; $i <= 10; $i++) {
            $logement = new Logement();
            $logement->setTypeLogement($this->getReference('type-' . rand(5, 7)))
                ->setUserId($this->getReference('user-admin'))
                ->setDescription($faker->paragraph(5, true))
                ->setFileName('default_c.png')
                ->setUpdateAt(new \DateTime('now'))
                ->setName($faker->name);

            $this->addReference('logement-' . ($i + 50), $logement);
            $manager->persist($logement);
        }

        for ($i = 1; $i <= 30; $i++) {
            $logement = new Logement();
            $logement->setTypeLogement($this->getReference('type-' . rand(8, 9)))
                ->setUserId($this->getReference('user-admin'))
                ->setDescription($faker->paragraph(5, true))
                ->setFileName('default_e.png')
                ->setUpdateAt(new \DateTime('now'))
                ->setName($faker->name);

            $this->addReference('logement-' . ($i + 60), $logement);
            $manager->persist($logement);
        }

        #endregion

        #region RÉSERVATIONS

        for ($i = 1; $i <= 16; $i++) {
            $reservation = new Reservation();
            $reservation->setLocataireId($this->getReference(('user-' . rand(1,20))))
                ->setLogementId($this->getReference('logement-' . rand(1, 90)))
                ->setCheckIn($faker->dateTimeBetween('2022-05-05', '2022-10-01'))
                ->setCheckOut($faker->dateTimeBetween('2022-05-20', '2022-10-10 '))
                ->setNbrEnfant(rand(0, 6))
                ->setNbrAdulte(rand(1, 5))
                ->setNbrSaison(rand(0, 20))
                ->setNbrHorsSaison(rand(0, 20))
                ->setJourPiscineEnfant(rand(0, 20))
                ->setJourPiscineAdulte(rand(0, 20));
            $this->addReference('reservation-' . $i, $reservation);
            $manager->persist($reservation);
        }

        #endregion


        #region Factures

        #endregion

        $manager->flush();
    }
}
