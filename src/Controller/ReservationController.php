<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Entity\logement;
use App\Entity\User;
use App\Form\ReservationType;
use App\Repository\LogementRepository;
use App\Repository\ReservationRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ReservationController extends AbstractController
{

    public function __construct(ReservationRepository $reservationRepository, LogementRepository $logementRepository)
    {
        $this->resaRepo = $reservationRepository;
        $this->logementRepo = $logementRepository;
    }


    /**
     * @Route("/reservation/{id}", name="app_detail", requirements={"id":"\d+"})
     */
    public function detail(Request $request, ManagerRegistry $manager): Response
    {
        $logement = $this->logementRepo->findBy(['id' => $request->get('id')]);
        $form = $this->createForm(ReservationType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $reservation = new Reservation();

            #region Vérification des dates pour les jour en saison ou pas

            // on enregistre directement le checkin vu qu'il va être moidifié
            $reservation->setCheckIn($form->getData()->getCheckIn());


            $debut_saison = new \DateTime('2022-05-05');
            $fin_saison = new \DateTime('2022-10-10');
            $nbr_saison = 0;
            $nbr_hors_saison = 0;

            $checkIn = $form->getData()->getCheckIn();
            $checkOut = $form->getData()->getCheckOut();

            $interval = $checkIn->diff($checkOut);

            for ($i = 0 ; $i <= $interval->d ; $i++){

                if($checkIn->getTimeStamp()> $debut_saison->getTimestamp() && $checkIn->getTimeStamp() < $fin_saison->getTimestamp()){
                    $nbr_saison = $nbr_saison+1;
                }else{
                    $nbr_hors_saison = $nbr_hors_saison + 1;
                }
                $checkIn->add(new \DateInterval('P1D'));
            }

            $reservation->setCheckOut($form->getData()->getCheckOut());
            $reservation->setNbrSaison($nbr_saison);
            $reservation->setNbrHorsSaison($nbr_hors_saison);

            #endregion


            $reservation->setLogementId($logement[0]);
            $reservation->setNbrEnfant($form->getData()->getNbrEnfant());
            $reservation->setNbrAdulte($form->getData()->getNbrAdulte());
            $reservation->setJourPiscineEnfant($form->getData()->getJourPiscineEnfant());
            $reservation->setJourPiscineAdulte($form->getData()->getJourPiscineAdulte());
            $reservation->setLocataireId($this->getUser());

            $em = $manager->getManager();
            $em->persist($reservation);
            $em->flush();

            $this->addFlash('success', 'La réservation à bien été effectuée');

            return $this->redirectToRoute('app_home', ['id' => $request->get('id')]);
        }


        return $this->render('form/detail.html.twig', [
            'logement' => $logement,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route(path="/admin/reservations", name="app_admin_reservations")
     * @return Response
     */
    public function ReservationListe(): Response
    {
        $reservations = $this->resaRepo->findAll();

        return $this->render('admin/admin_reservations.html.twig', [
            'reservations' => $reservations,
            'role' => 'admin'
        ]);
    }


    /**
     * @Route(path="/proprietaire/reservations", name="app_owner_reservations")
     * @return Response
     */
    public function ReservationListeProprio(): Response
    {

        $reservation = $this->resaRepo->findAllProprioResa($this->getUser());

        return $this->render('admin/admin_reservations.html.twig', [
            'reservations' => $reservation,
            'role' => 'owner'
        ]);
    }

    /**
     * @Route(path="/admin/reservations/modifications/{id}", name="app_admin_resa_modif", methods={"GET","POST"},requirements={"id":"\d+"})
     *
     * @return void
     */
    public function modifReservation(int $id, Request $request, ManagerRegistry $manager)
    {
        $reservation = $this->resaRepo->find($id);
        $formulaire = $this->createForm(ReservationType::class, $reservation);
        $formulaire->handleRequest($request);

        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $manager = $manager->getManager();
            $manager->persist($reservation);
            $manager->flush();
            $this->addFlash('success', 'La modification a bien été effectuée');
            return $this->redirectToRoute("app_admin_reservations");
        }

        return $this->render('admin/admin_modif_resa.html.twig', [
            'form' => $formulaire->createView(),
        ]);
    }

    /**
     * @Route(path="/admin/reservation/supprimer/{id}", name="app_admin_resa_supp", methods={"POST"})
     * @param int $id
     * @param Request $request
     * @param ManagerRegistry $manager
     *
     * @return RedirectResponse|Response
     */
    public function suppressionReservation(int $id, Request $request, ManagerRegistry $manager)
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->get('_token'))) {
            $em = $manager->getManager();
            $reservation = $this->resaRepo->find($id);
            $em->remove($reservation);
            $em->flush();
            $this->addFlash('success', 'La suppression a bien été effectuée');
            return $this->redirectToRoute('app_admin_reservations');
        } else {
            return new Response("<h1><i class='fa-solid fa-skull-crossbones'></i></h1>");
        }
    }


}
