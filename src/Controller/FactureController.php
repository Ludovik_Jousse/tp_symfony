<?php

namespace App\Controller;

use App\Entity\Facture;
use App\Entity\LigneDeFacturation;
use App\Entity\Reservation;
use App\Entity\Tarifs;
use App\Repository\FactureRepository;
use App\Repository\LigneDeFacturationRepository;
use App\Repository\ReservationRepository;
use App\Repository\TarifsRepository;
use Doctrine\Persistence\ManagerRegistry;
use Faker\Provider\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FactureController extends AbstractController
{

    public function __construct(FactureRepository $factureRepository, TarifsRepository $tarifsRepository, LigneDeFacturationRepository $ligne, ReservationRepository $reservationRepository)
    {
        $this->tarifRepo = $tarifsRepository;
        $this->ligneRepo = $ligne;
        $this->factureRepo = $factureRepository;
        $this->resaRepo = $reservationRepository;
    }

    /**
     * @Route("/admin/factures", name="app_facture")
     */
    public function index(): Response
    {
        $ids = [];

        $factures = $this->factureRepo->findAll();

        $res = $this->calculsFacture([$factures]);

        foreach ($factures as $k) {
            $ids[] = $k->getId();
        }


        return $this->render('admin/admin_factures.html.twig', [
            'resultats' => $res,
            'date' => new \DateTime('now'),
            'factureId' => $ids,
            'page' => 'liste'
        ]);
    }

    /**
     * @Route(path="/admin/factures/ajout/{id}", name="app_admin_facture_ajout")
     * @return Response
     */
    public function ajoutFacture(int $id, ManagerRegistry $manager)
    {

        $facture = new Facture();

        $tarifs = $this->tarifRepo->findAll();

        $reservation = $this->resaRepo->find($id);

        $nbrTotal = intval(($reservation->getNbrHorsSaison() + $reservation->getNbrSaison()));

        $arr = array_column($tarifs, 'prix', 'nom');

        $type = $reservation->getLogementId()->getTypeLogement()->getId();

        $labels = [
            [
                'label' => 'Nombre de jours hors saison',
                'quantite' => $reservation->getNbrHorsSaison(),
                'prix' => $arr[$type],
                'facture' => $facture
            ],

            [
                'label' => 'Nombre de jours saison',
                'quantite' => $reservation->getNbrSaison(),
                'prix' => $arr[$type],
                'facture' => $facture
            ],

            [
                'label' => 'Taxe Séjour Enfant',
                'quantite' => $reservation->getNbrEnfant(),
                'prix' => $arr['TSEnfant'],
                'facture' => $facture
            ],

            [
                'label' => 'Taxe Séjour Adulte',
                'quantite' => $reservation->getNbrAdulte(),
                'prix' => $arr['TSAdulte'],
                'facture' => $facture
            ],

            [
                'label' => 'Piscine Enfant',
                'quantite' => $reservation->getJourPiscineEnfant(),
                'prix' => $arr['PEnfant'],
                'facture' => $facture
            ],

            [
                'label' => 'Piscine Adulte',
                'quantite' => $reservation->getjourPiscineAdulte(),
                'prix' => $arr['PAdulte'],
                'facture' => $facture
            ],
        ];


        $this->addLigneDeFacturation($labels, $facture, $manager);

        $em = $manager->getManager();
        $em->persist($facture);

        $em->flush();

        $res = $this->calculsFacture([[$facture]]);


        return $this->render('admin/admin_ajout_facture.html.twig', [
            'factures' => $facture,
            'resultats' => $res,
            'date' => new \DateTime('now'),
            'factureId' => [$facture->getId()],
            'page' => 'ajout'
        ]);


    }

    /** Fonction qui permet de faire tous les calculs d'une facture et qui renvoie les valeurs dans un tableau
     *
     * @param array $facture
     *
     * @return array
     */
    protected function calculsFacture(array $facture): array
    {
        $arrTest = [];
        $resultat = [];

        foreach ($facture as $item) {

            foreach ($item as $key) {
                $lignes = $key->getLigneDeFacturation()->getValues();
                $compteur = 0;
                $remise = 0;
                $sousTotal = 0;

                foreach ($lignes as $k => $v) {


                    $arrTest[$k]['label'] = $v->getLabel();
                    $arrTest[$k]['quantite'] = $v->getQuantite();

                    if ($v->getLabel() == 'Nombre de jours saison') {

                        $arrTest[$k]['prixUnitaire'] = $v->getPrixUnitaire() + $v->getPrixUnitaire() * 15 / 100;
                        $arrTest[$k]['total'] = ($v->getPrixUnitaire() * $v->getQuantite()) + (($v->getPrixUnitaire() * $v->getQuantite()) * 15 / 100);

                        if ($v->getQuantite() / 7 > 0) {
                            $remise = ($v->getQuantite() - ($v->getQuantite() % 7));
                            $nbrRemise = $remise / 7;
                            $remise = (($remise * $v->getPrixUnitaire()) * 5) / 100;
                        }

                    } else {
                        $arrTest[$k]['prixUnitaire'] = $v->getPrixUnitaire();
                        $arrTest[$k]['total'] = $v->getPrixUnitaire() * $v->getQuantite();
                    }

                    $compteur++;

                    $sousTotal = $sousTotal + $arrTest[$k]['total'];
                }

                $total = $sousTotal - $remise;

                $arrTest[$compteur] = [
                    'label' => 'Sous-total',
                    'quantite' => '',
                    'prixUnitaire' => '',
                    'total' => $sousTotal
                ];

                if ($remise != 0) {
                    $arrTest[$compteur + 1] = [
                        'label' => 'remise',
                        'quantite' => $nbrRemise,
                        'prixUnitaire' => '5%',
                        'total' => -$remise
                    ];

                } else {

                    $arrTest[$compteur + 1] = [
                        'label' => 'remise',
                        'quantite' => 0,
                        'prixUnitaire' => '5%',
                        'total' => 0
                    ];
                }

                $arrTest[$compteur + 2] = [
                    'label' => 'TOTAL',
                    'quantite' => '',
                    'prixUnitaire' => '',
                    'total' => $total
                ];


                $resultat[] = $arrTest;

            }
        }


        return $resultat;
    }

    /**
     * @Route(path="/proprietaire/facture", name="app_owner_facture")
     * @return void
     */
    public function factureProprio(ManagerRegistry $manager)
    {
        $reservation = $this->resaRepo->findAllProprioResa($this->getUser());

        $arrFull = [];
        $total =  [];
        $i = 0;
        $nomLogement = [];

        foreach ($reservation as $key => $resa) {

            $facture = new Facture();

            $tarifs = $this->tarifRepo->findAll();

            $nbrTotal = intval(($resa->getNbrHorsSaison() + $resa->getNbrSaison()));

            $arr = array_column($tarifs, 'prix', 'nom');

            $type = $resa->getLogementId()->getTypeLogement()->getId();

            $labels = [
                [
                    'label' => 'Nombre de jours hors saison',
                    'quantite' => $resa->getNbrHorsSaison(),
                    'prix' => $arr[$type],
                    'facture' => $facture
                ],

                [
                    'label' => 'Nombre de jours saison',
                    'quantite' => $resa->getNbrSaison(),
                    'prix' => $arr[$type],
                    'facture' => $facture
                ],
            ];


            $this->addLigneDeFacturation($labels, $facture, $manager);


            $arrFull[$key] = $this->calculsFacture([[$facture]]);

            $nomLogement[] = $reservation[$key]->getLogementId()->getName();

        }


        $em = $manager->getManager();
        $em->persist($facture);

        $em->flush();


        $totalString = 0;

        foreach ($arrFull as $item){

            foreach ($item as $kitem => $value){
                $i++;

                $total[$i]['label'] =  $nomLogement[$i-1];
                $total[$i]['prix'] =  $value[2]['total'];

                $totalString = $totalString + $value[2]['total'];
            }
        }
        $total[$i+1]['label'] = 'SOUS-TOTAL';
        $total[$i+1]['prix'] = $totalString;

        $total[$i+2]['label'] = 'RETRIBUTION';
        $total[$i+2]['prix'] = $totalString * 0.35;

        $total[$i+3]['label'] = 'TOTAL';
        $total[$i+3]['prix'] = $totalString - $total[$i+2]['prix'];


        return $this->render('admin/owner_facture.html.twig',[
            'resultats' => [$total],
            'date' => new \DateTime('now'),
            'factureId' => [$facture->getId()],
            'page' => 'liste'
        ]);

    }


    public function addLigneDeFacturation(array $labels, Facture $facture, ManagerRegistry $manager)
    {
        foreach ($labels as $k => $v) {

            $ligne = new LigneDeFacturation();

            $facture->setDateCreation(new \DateTime('now'));

            $facture->addLigneDeFacturation(
                $ligne->setLabel($v['label'])
                    ->setQuantite($v['quantite'])
                    ->setPrixUnitaire($v['prix'])
                    ->setFacture($facture)
            );

        }

    }
}
