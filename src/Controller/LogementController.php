<?php

namespace App\Controller;

use App\Entity\Logement;
use App\Entity\Reservation;
use App\Entity\TypeLogement;
use App\Form\ReservationType;
use App\Repository\LogementRepository;
use App\Repository\TypeLogementRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogementController extends AbstractController
{

    public function __construct(LogementRepository $logement, TypeLogementRepository $typeLogement)
    {
        $this->logementRepo = $logement;
        $this->typeRepo = $typeLogement;
    }

    /**
     * @Route("/logement", name="app_logement")
     */
    public function index(): Response
    {
        return $this->render('logement/Liste.html.twig', [
            'controller_name' => 'LogementController',
        ]);
    }


    /**
     * @Route(path="/logement/{page}" , name="app_type", methods={"GET", "POST"} )
     */
    public function typeLogement(string $page): Response
    {
        switch ($page) {
            case 'mobilHomes':
                $res = $this->logementRepo->findByType($this->typeRepo->findMobilHomeId());
                break;
            case 'caravanes':
                $res = $this->logementRepo->findByType($this->typeRepo->findCaravaneId());
                break;
            case 'emplacements':
                $res = $this->logementRepo->findByType($this->typeRepo->findEmplacementId());
                break;
        }

        return $this->render('logement/detail.html.twig', [
            'res' => $res,
        ]);
    }



}
