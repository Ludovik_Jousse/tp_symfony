<?php

namespace App\Controller;

use App\Entity\Facture;
use App\Entity\LigneDeFacturation;
use App\Repository\FactureRepository;
use App\Repository\LigneDeFacturationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{

    public function __construct(FactureRepository $factureRepository, LigneDeFacturationRepository $ligneDeFacturationRepository)
    {
        $this->factureRepo = $factureRepository;
        $this->ligneRepo = $ligneDeFacturationRepository;
    }

    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [

        ]);
    }

    /**
     * @Route(path="/login", name="app_connexion")
     * @param AuthenticationUtils $utils
     *
     * @return Response
     */
    public function connexion(AuthenticationUtils $utils): Response
    {
        $error = $utils->getLastAuthenticationError();
        $lastIdent = $utils->getLastUsername();

        return $this->render('user/user.html.twig', [
            'lastIdent' => $lastIdent,
            'errors' => $error
        ]);
    }


    /**
     * @Route(path="/logout", name="app_logout")
     * @return void
     */
    public function logout()
    {
        $this->redirectToRoute('app_home');
    }


    /**
     * @Route(path="/admin/home", name="app_admin_dashboard", methods={"GET","POST"})
     * @return Response
     */
    public function adminDashboard()
    {
        return $this->render('admin/admin_dashboard.html.twig', [

        ]);
    }

    /**
     * @Route(path="/proprietaire/dashboard", name="app_owner_dashboard")
     * @return void
     */
    public function ownerDashboard()
    {

        return $this->render('owner/owner_dashboard.html.twig');
    }


    /**
     * @Route(path="/verifications" , name="app_verification")
     * @return RedirectResponse
     */
    public function middlewareConnexion(): RedirectResponse
    {

        return ($this->getUser()->getRoles()[0] == 'ROLE_ADMIN' ) ? $this->redirectToRoute('app_admin_dashboard'):
            $this->redirectToRoute('app_owner_dashboard');
    }

}
