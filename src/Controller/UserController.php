<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    /**
     * @Route("/user", name="app_user")
     */
    public function index(): Response
    {
        return $this->render('user/user.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route(path="/admin/clients", name="app_admin_clients")
     * @return void
     */
    public function listeClient()
    {
        $data = $this->userRepo->findAll();

        return $this->render('admin/admin_client_list.html.twig', [
            'resultats' => $data,
            'role' => 'owner'
        ]);
    }

    /**
     * @Route(path="/admin/clients/supprimer/{id}", name="app_admin_client_supp", methods={"POST"})
     * @param int $id
     * @param Request $request
     * @param ManagerRegistry $manager
     *
     * @return RedirectResponse|Response
     */
    public function suppressionUser(int $id, Request $request,ManagerRegistry $manager)
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->get('_token'))) {
            $em = $manager->getManager();
            $user = $this->userRepo->find($id);
            $em->remove($user);
            $em->flush();
            $this->addFlash('success', 'La suppression a bien été effectuée');
            return $this->redirectToRoute('app_admin_clients');
        } else {
            return new Response("<h1><i class='fa-solid fa-skull-crossbones'></i></h1>");
        }
    }

}
