<?php

namespace App\Entity;

use App\Repository\LogementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=LogementRepository::class)
 * @Vich\Uploadable()
 */
class Logement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="logementId")
     */
    private $reservations;

    /**
     * @ORM\ManyToOne(targetEntity=TypeLogement::class, inversedBy="logements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeLogement;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="logements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;



    /**
     * @ORM\Column (type="string", length=255)
     * @var string\null
     */
    private $fileName;

    /**
     * @var File\null
     * @Vich\UploadableField(mapping="camping_image",fileNameProperty="fileName")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date")
     */
    private $updateAt;



    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string\null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     * @return Logement
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return File
     */
    public function getImagefile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File $imageFile
     */
    public function setImagefile(?File $imageFile): ?self
    {
        $this->imageFile = $imageFile;
        if($this->imagefile instanceof UploadedFile){
            $this->updateAt = new \DateTime('now');
        }
        return $this;
    }


    public function getUpdateAt() : ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt($updateAt): self
    {
        $this->updateAt = $updateAt;
        return $this;
    }


    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setLogementId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getLogementId() === $this) {
                $reservation->setLogementId(null);
            }
        }

        return $this;
    }

    public function getTypeLogement(): ?typeLogement
    {
        return $this->typeLogement;
    }

    public function setTypeLogement(?typeLogement $typeLogement): self
    {
        $this->typeLogement = $typeLogement;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

}
