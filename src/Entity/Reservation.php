<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Logement;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $checkIn;

    /**
     * @ORM\Column(type="date")
     */
    private $checkOut;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrEnfant;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrAdulte;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrSaison;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrHorsSaison;

    /**
     * @ORM\Column(type="integer")
     */
    private $jourPiscineEnfant;

    /**
     * @ORM\Column(type="integer")
     */
    private $jourPiscineAdulte;

    /**
     * @ORM\ManyToOne(targetEntity=Logement::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $logementId;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $locataireId;


    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->logementId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCheckIn(): ?\DateTimeInterface
    {
        return $this->checkIn;
    }

    public function setCheckIn(\DateTimeInterface $checkIn = null): self
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    public function getCheckOut(): ?\DateTimeInterface
    {
        return $this->checkOut;
    }

    public function setCheckOut(\DateTimeInterface $checkOut = null): self
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    public function getNbrEnfant(): ?int
    {
        return $this->nbrEnfant;
    }

    public function setNbrEnfant(int $nbrEnfant): self
    {
        $this->nbrEnfant = $nbrEnfant;

        return $this;
    }

    public function getNbrAdulte(): ?int
    {
        return $this->nbrAdulte;
    }

    public function setNbrAdulte(int $nbrAdulte): self
    {
        $this->nbrAdulte = $nbrAdulte;

        return $this;
    }

    public function getNbrSaison(): ?int
    {
        return $this->nbrSaison;
    }

    public function setNbrSaison(int $nbrSaison): self
    {
        $this->nbrSaison = $nbrSaison;

        return $this;
    }

    public function getNbrHorsSaison(): ?int
    {
        return $this->nbrHorsSaison;
    }

    public function setNbrHorsSaison(int $nbrHorsSaison): self
    {
        $this->nbrHorsSaison = $nbrHorsSaison;

        return $this;
    }

    public function getJourPiscineEnfant(): ?int
    {
        return $this->jourPiscineEnfant;
    }

    public function setJourPiscineEnfant(int $jourPiscineEnfant): self
    {
        $this->jourPiscineEnfant = $jourPiscineEnfant;

        return $this;
    }

    public function getjourPiscineAdulte(): ?int
    {
        return $this->jourPiscineAdulte;
    }

    public function setJourPiscineAdulte(int $jourPiscineAdulte): self
    {
        $this->jourPiscineAdulte = $jourPiscineAdulte;

        return $this;
    }

    public function getLogementId(): ?object
    {
        return $this->logementId;
    }

    public function setLogementId(object $logementId): self
    {
        $this->logementId = $logementId;

        return $this;
    }

    public function getLocataireId(): ?User
    {
        return $this->locataireId;
    }

    public function setLocataireId(?User $locataireId): self
    {
        $this->locataireId = $locataireId;

        return $this;
    }

}
