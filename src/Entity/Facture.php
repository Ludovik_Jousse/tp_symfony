<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @ORM\OneToMany(targetEntity=LigneDeFacturation::class, mappedBy="facture"  , cascade={"persist"})
     */
    private $LigneDeFacturation;

    public function __construct()
    {
        $this->LigneDeFacturation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return Collection<int, LigneDeFacturation>
     */
    public function getLigneDeFacturation(): Collection
    {
        return $this->LigneDeFacturation;
    }

    public function addLigneDeFacturation(LigneDeFacturation $ligneDeFacturation): self
    {
        if (!$this->LigneDeFacturation->contains($ligneDeFacturation)) {
            $this->LigneDeFacturation[] = $ligneDeFacturation;
            $ligneDeFacturation->setFacture($this);
        }

        return $this;
    }

    public function removeLigneDeFacturation(LigneDeFacturation $ligneDeFacturation): self
    {
        if ($this->LigneDeFacturation->removeElement($ligneDeFacturation)) {
            // set the owning side to null (unless already changed)
            if ($ligneDeFacturation->getFacture() === $this) {
                $ligneDeFacturation->setFacture(null);
            }
        }

        return $this;
    }


}
