<?php

namespace App\Form;

use App\Entity\Logement;
use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('checkIn', DateType::class, [
            'widget' => 'single_text',
            'data' => new \DateTime(),
            'attr' => ['class' => 'form-control'],
            'by_reference' => true,
            'required' => false
        ])
            ->add('checkOut', DateType::class, [
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'attr' => ['class' => 'form-control'],
                'by_reference' => true,
                'required' => false
            ])
            ->add('nbrEnfant', null, [
                'label' => "nombre d'enfant",
                'attr' => ["placeholder" => 'enfants', 'class' => 'form-control'],
                'required' => true
            ])
            ->add('nbrAdulte', null, [
                'label' => "nombre d'adultes",
                'attr' => ['placeholder' => 'adultes', 'class' => 'form-control'],
                'required' => true
            ])
            ->add('jourPiscineEnfant', null, [
                'label' => "nombre de jour pour la piscine enfant",
                'attr' => ['placeholder' => 'piscine enfant', 'class' => 'form-control'],
                'required' => true
            ])
            ->add('jourPiscineAdulte', null, [
                'label' => "nombre de jour pour la piscine enfant",
                'attr' => ['placeholder' => 'piscine adulte', 'class' => 'form-control'],
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
